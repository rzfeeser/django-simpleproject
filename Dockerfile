# This base image container is avail on hub.docker.com
# it has python 3.16 avail on Alpine Linux, a minimalist Linux distro
FROM python:alpine3.16
COPY . /app
WORKDIR /app
# Use Python package installer to install the gunicorn library
RUN pip install -r requirements.txt
# container is exposed on port 8000
EXPOSE 8000
CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
