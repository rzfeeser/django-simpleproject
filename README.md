# Django Simple Project - CI Pipeline

### Project Description

This GitLab project contains 3 learning objectives:

1. A simple Django project (as simple as I could make it). It has two endpoints:
- `/say-hello/` - This returns HTTP 200 + `Hello World!`
- `/admin/` - This return HTTP 200 + HTML (access the admin portal)

2. A `Dockerfile`, which builds the project into a container. Once the project is a container, the container may be launched from *anywhere* Docker is installed, (this would be inclusive of Kubernetes environments). Containers also have the benefit of including everything our project needs to run (i.e. Python, Django, and other dependencies)

3. The CI portion of the project is driven by a file named `.gitlab-ci.yml`. This file describes what GitLab needs to do on our behalf everytime a change is made to our repo (development). In the case of this project, a change will provoke GitLab to auto run the `Dockerfile`, and then PUSH the resultant image to **Packages and registries** >> **Container Registry**. The pushed image is then switched on, and an HTTP GET is sent (by cURL) to the endpoint `/say-hello/`.

In conclusion, if the pipeline is showing **SUCCESS**, we can be confident that our container is ready to deploy. Of course, if any of these steps fail, our project will alert us to the failure, and we can take steps to stablize the pipeline (i.e the process that creates our Django container image).

### Contents

This GitLab project contains the following:
- Django project named `simpleproject`
- `Dockerfile` - Describes creating the container
- `.gitlab-ci.yml` - This file enables CI scripting within GitLab. Currently this file has two stages
  1. `build` - This stage executes the Dockerfile, create the container, and pushes it to the GitLab container repository
  2. `acceptance` - This stage downloads the image `curlimages/curl` from [https://hub.docker.com/r/curlimages/curl](https://hub.docker.com/r/curlimages/curl) the offical image for "curling" and uses it to make a request against the image artifact from the `build` stage

### Running the Container

From any place you have docker installed, you should be able to run the container with the following command *you may need to run the command as "sudo" depending on your setup*:

```
## Command Description ##
# docker run = run this container
# -d = run in background
# --name = a name (handle / alias) for the container
# -p 8000:8000 = The port 8000 on the LEFT is the port on the LOCAL/ROOT machine
#                the port 8000 on the RIGHT is the port we are sending to inside the CONTAINER
#                (this should match the port our container runs on)
# registry.gitlab.com/rzfeeser/django-simpleproject = use this image (it will be downloaded)


docker run -d --name rzfeeser_simpleproject -p 8000:8000 registry.gitlab.com/rzfeeser/django-simpleproject
```


### Author
@RZFeeser
